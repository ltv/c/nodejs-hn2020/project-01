const express = require('express');
const router = express.Router();
const { createTodo , clearCompleted ,updateTodo, deleteTodo, getTodoList} = require("../controllers/TodoController");

router.post('/createTodo', createTodo);
router.post('/updateTodo', updateTodo);
router.post('/todos', getTodoList);
router.post('/deleteTodo',deleteTodo);
router.post('/clearCompleted',clearCompleted);

module.exports = router;
